FROM ubuntu:14.04.5
MAINTAINER tan.mng90@gmail.com

ENV DRAW_TOOL_LOCATION /opt/draw

# Install required packages
RUN apt-get update && apt-get install -y \
    curl

# install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
RUN apt-get install -y nodejs

# Install required things
RUN mkdir -p ${DRAW_TOOL_LOCATION}
ADD ./server.js ${DRAW_TOOL_LOCATION}/server.js
ADD ./package.json ${DRAW_TOOL_LOCATION}/package.json

ADD ./public ${DRAW_TOOL_LOCATION}/public

EXPOSE 8080

# Install required packages
WORKDIR ${DRAW_TOOL_LOCATION}
RUN npm install

# Run command on container startup
CMD nodejs server.js
